<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'local_bstudio_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'little-chompy');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dr :r-V]y&-<,v@-E;2,Sc27e];[h1@+TAx$t=a0k4-pM4M`08|fn%zc_;{&XY0 ');
define('SECURE_AUTH_KEY',  'Z~]<Nj=OA1wyH@mB;TpL E*b8J~MipX@U3jyE%rW]#l^m#~pdBqMX=_pq4$+QS|;');
define('LOGGED_IN_KEY',    '_xh4]>+Ba1,$06-ks yoFaYKs,.Z8fr+@:,ffmco,30Ej1%81Lx/|Ed=~+RImiWn');
define('NONCE_KEY',        '4jw/3<iz qdttAtmJ+)|;tvjYLt|@e?%g3wPRdi<xa(Jt>c3w!eHf&:s>S}c}s,u');
define('AUTH_SALT',        'DPcFy8`rq%sV=:}DeY&5@|)J).|e<FzyyY$2 u_$szX<L5bL9kmPSmAdXS`vm0ut');
define('SECURE_AUTH_SALT', '5IBo49w&m&0jBMiXt3-4/v|65Hfa#HO//B-J!L5+mkI2l?(Q[Blw$+KOV+8{d*r.');
define('LOGGED_IN_SALT',   '9%en[1T[UY1a_=vD?,{j5!6wlnKsv83:EyPT6Qs8]v_YRnH!dc3{/C3-%{9hsl,v');
define('NONCE_SALT',       '7YfOc{@y1Cn{s^~KlO5gSX;{Ss<v<_O_I;L|<BMx$]cmJ0p{VaTDAryQHbla5$wZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
