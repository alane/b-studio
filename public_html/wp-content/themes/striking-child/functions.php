<?php 
/* Load the Theme class. */
require_once (get_stylesheet_directory() . '/framework/theme.php');

$theme = new Theme();
$options = include(get_template_directory() . '/framework/info.php');
$options['theme_child_name'] = 'Striking Child';
$options['theme_slug'] = 'striking';

$theme->init($options);

function theme_require_once($file){
	$child_file = str_replace(get_template_directory(),get_stylesheet_directory(),$file);
	if(file_exists($child_file)){
		require_once($child_file);
	}else{
		require_once($file);
	}
}

function theme_enqueue_child_style(){
	wp_enqueue_style('theme-child-style', get_stylesheet_directory_uri(). '/style.css',array('theme-style','theme-skin'),false,'all');
}
add_action('wp_print_styles', 'theme_enqueue_child_style');

function theme_child_cufon_fonts($fonts){
	$child_fonts = array();
	$font_files = glob(THEME_CHILD_FONT_DIR."/*.js");
	if(!empty($font_files)){
		foreach($font_files as $font_file){
			$file_content = file_get_contents($font_file);
			if(preg_match('/font-family":"(.*?)"/i',$file_content,$match)){
				$file_name = basename($font_file);
				$child_fonts[$file_name] = array(
					'font_name' => $match[1],
					'file_name' => $file_name,
					'url' => THEME_CHILD_FONT_URI.'/'.$file_name
				);
			}
		}
	}
	$fonts = array_merge($fonts, $child_fonts);
	return $fonts;
}
add_filter('theme_cufon_fonts', 'theme_child_cufon_fonts');

function theme_child_fontface_fonts($fonts){
	$child_fonts = array();
	$fontface_dir_files = glob(THEME_CHILD_FONTFACE_DIR.'/*');
	if(!empty($fontface_dir_files)){
		$font_dirs = array_filter($fontface_dir_files, 'is_dir');
		if(!empty($font_dirs)){
			foreach($font_dirs as $dir){
				$stylesheet = $dir.'/stylesheet.css';
				if(file_exists($stylesheet)){
					$file_content = file_get_contents($stylesheet);
					if( preg_match_all("/@font-face\s*{.*?font-family\s*:\s*('|\")(.*?)\\1.*?}/is", $file_content, $matchs) ){
						foreach($matchs[0] as $index => $css){
							$font_folder = basename($dir);
							$child_fonts[$font_folder.'|'.$matchs[2][$index]] = array(
								'folder' => $font_folder,
								'name' => $matchs[2][$index],
								'css' => $css,
								'dir' => THEME_CHILD_FONTFACE_DIR. '/'.$font_folder.'/stylesheet.css',
								'url' => THEME_CHILD_FONTFACE_URI. '/'.$font_folder.'/stylesheet.css',
							);
						}
					}
				}
			}
		}
	}

	$fonts = array_merge($fonts, $child_fonts);
	return $fonts;
}
add_filter('theme_fontface_fonts', 'theme_child_fontface_fonts');

/*
You can add your custom functions below 
*/