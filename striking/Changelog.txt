﻿Version 5.2.2.2 - Sep.27th ‘13
===============================================
Fixed : Ajax portfolio lightbox issue
Fixed : Shortcode generator notice text issue
Changed : Breadcrumbs so that Woo Breadcrumbs defaults to WP SEO breadcrumbs

Version 5.2.2.1 - Sep.24th ‘13
===============================================
Fixed : Nivo slider shrotcode navigation control issue
Fixed : Select2 document click not working issue

Version 5.2.2 - Sep.23th ‘13
===============================================
Improved : Woocommerce support
Added : The Events Calendar Plugin compatibility
Added : Codecanyon DZS Timeline Slider Compatibility
Added : Custom Post Types Categories Filter
Added : target option for portfolio list widget
Added : Archives custom text shortcode support
Fixed : Accordion title color option issue
Fixed : Walker start_el warning info issue
Fixed : shortcoded slider with full post list issue
Fixed : bbpress breadcrumb issue
Fixed : empty href button shortcode on ios safari issue
Fixed : toggle with gmap issue
Fixed : portfolio ajax group issue
Fixed : wpml with front page issue
Updated : mediaelement scripts
Updated : colorbox scripts
Updated : imagesloaded scripts

Version 5.2.1 - July.18th ‘13
===============================================
Added : Yoast breadcrumb support
Added : Woo breadcrumb support
Added : image trigger for lightbox shortcode
Added : alt tag support for image shortcode
Fixed : blog feature image width issue  (reported by “Torstein”)
Fixed : fix search sidebar issue when no custom sidebar assign
Fixed : corrected a portfolio id issue in the single portfolio template
Fixed : fix blog shortcode column issue
Fixed : fix nivo slider control navigation icons placement issue
Fixed : fallout submenu on rtl websites issue
Fixed : google font subset warn issue (reported by lakleeman & Starrgirrl)
Fixed : Anything Slider sidebar in shortcode with oversized sidebar (reported by Olivier)
Fixed : Blog shortcode pagination when shortcode used within a single blog post body
Fixed : Edit of Woo product page
Changed : Wording of Blog shortcode help fields for Category and Multiple Category settings
Updated : select2 scripts
Updated : colorbox scripts

Version 5.2.0 - June.28th ‘13
===============================================
Updated : child theme
Updated : woocommerce 2.0 compatible
updated : mediaelement script
updated : sticky footer script
Updated : colorbox lightbox script
Updated : gmap script
Updated : Anything slider script
Updated : accordion slider script
Updated : google font function
Updated : twitter widget oauth support
Fixed : umlauts (ë í ä ) in tabs title with history = on issue
Fixed : wpmu get_image_src strpos issue
Fixed : mb_substr error warning
Fixed : image loading function issue for ie 10
Fixed : blog readmore button option issue
Fixed : blog shortcode offset issue
Fixed : nivo slider opacity control issue
Fixed : new media panel uploading image function issue
Fixed : Portfolio ajax function issue
Added : Archive Type Pages Custom Sidebar
Added : Fixed Height for featured image in single post page
Added : sticky header support
Added : filter for theme options array
Added : flv support for html5 video shortcode generator
Added : Romanian language

Version 5.1.9.6 - Jan.21th ‘13
===============================================
Added : feature image option for single portfolio page
Added : portfolio display as button attribute for shortcode
Fixed : nivo slider caption opacity issue
Fixed : kwick slider frame border issue
Fixed : archive filter notice warning issue
Fixed : search widget shortcode button text issue
Fixed : custom post type feature text settings issue
Removed: editor style css
Updated : Anything slider js
Updated : nivo slider js
Updated : colorbox js

Version 5.1.9.5 - Jan.7th ‘13
===============================================
Fixed : complex css option issue
Fixed : alt tag for shortcode slideshow
Fixed : portfolio flush rewrite rules issue
Fixed : navigation cufon font hover issue
Fixed : comment page warning text issue
Fixed : nivo slider caption issue
Fixed : read more issue in archive page
Fixed : feature image issue in single post page
Fixed : wp 3.5 gallery shortcode issue
Fixed : button shortcode hover color issue
Fixed : Admin translation for Striking backend in native language rendering
Fixed : Lightbox Shortcode
Fixed : Lightbox border display error
Fixed : Lightbox Shortcode caption centering
Fixed : Nivo Slider transition issue when using transparent images
Fixed : Font panel js error causing panel to crash
Fixed : Cufon menu dropdown in IE 10
Fixed : Use of simple html in nivo slider caption when in page content
Fixed : Static homepage custom title and custom text not showing when page set as static homepage
Fixed : Cufon boldness in top level navigation
Fixed : Double Cufon hover for top level navigation with a non-static homepage
Fixed : Anything Slider stop while playing Vimeo and HTML 5 video - youtube remains outstanding
Fixed : Menu exclusion setting for Striking menu
Fixed : Gallery function with new WP 3.5 uploader
Added : Anything Slider caption text Setting
Added : Anything Slider caption background color setting
Added : read more button padding for blog indexpage while frame mode is used
Added : read more button override in the bloglist shortcode
Removed : Stop Autoplay settings in Anything Slider Metabox
Changed : CSS position for the Anything Slider Sidebar
Changed : Updated scripting for caption and description usage for all slide types
Updated : slideshow metabox panel updated with new help fields
Updated : advanced, color, font, general, sidebar and slideshow theme panels updated with new help fields
Updated : layout_sprites.png
Updated : use wp 3.5 media uploader
Updated : translation pot file
Updated : Anything slider js
Updated : quicksand js
Updated : tweet js script
Updated : mediaelement js script
Updated : Striking compatiblity for Woo-Commerce 2.0 Beta
Updated : IE 8 compatibility for new navigation scripting

Plus uncounted code refinements and tweaks in the hundreds.  Striking should load extremely fast as a result.

Version 5.1.9.3 - Dec.20th ‘12
===============================================
Fixed : Nivo slider caption issue
Fixed : nivo hide next & pre button option issue
Added : Hide Control Navigation option for Nivo slider
Fixed : some cufon font issue
Fixed : blog single page issue
Fixed : portfolio single page issue
Fixed : portfolio restrict image lightbox dimension option issue
Fixed : contact us widget icon issue

Version 5.1.9.2 - Dec.19th ‘12
===============================================
Fixed : metabox issue
Fixed : errors when unpacking the package (for future upgrade)

Version 5.1.9.1 - Dec.19th ‘12
===============================================
Fixed : homepage feature area issue
Fixed : font option issues
Fixed : nivo slideshow caption font issue
Updated : child theme

Version 5.1.9 - Dec.18th ‘12
===============================================
Improved : admin design
Fixed : some admin option issue
Added : current language class to body tag
Added : fax filed for contact info widget
Added : frame color options for blog shortcode
Added : caption option for image shortcode
Added : restrict to screen option for lightbox
Added : some new options for advanced divider shortcode
Updated : youtube shortcode
Updated : mediaelement script
Updated : tweets script
Updated : nivo slider script
Updated : quicksand script
Updated : kwicks script
Updated : colorbox script
Updated : anythingslider script
Updated : navigation script
Updated : chosen script
Updated : jquery script
Fixed : exclude category issue
Fixed : image resize issue

Version 5.1.8.3 - July.6th ‘12
===============================================
Fixed : search button shortcode issue
Fixed : page navigation font size issue
Fixed : 'Enable image link lightbox' option issue
Fixed : titlelinkable attribute issue for portfolio shortcode
Fixed : same image name cache overwrite issue
Fixed : 3d Image Rotator with self gallery issue
Fixed : Featured image lightbox caption issue
Added : author name link to its posts instead of website
Updated : AnythingSlider scripts

Version 5.1.8.2 - June.15th ‘12
===============================================
Fixed : wp 3.4 upload option issue
Fixed : wp 3.4 contact form layout issue
Fixed : portfolio ajax list issue
Added : recent & related blog/portfolio widget custom thumbnail support
Added : custom Read More text support
Updated : AnythingSlider scripts
Updated : MediaElement.js scripts

Version 5.1.8.1 - May.21th ‘12
===============================================
Added : Graysale image hover effect animation speed Settings
Added : read_more attribute for blog shortcode
Added : Category Previous & Next Navigation for blog
Fixed : exclude Categories for Blog Previous & Next Navigation
Fixed : woocommerce plugin breadcrumb styling compatibility 
Fixed : comment form button color issue
Updated : AnythingSlider scripts
Updated : sticky footer script
Updated : MediaElement.js scripts
Updated : jQuery gMap script
Updated : Child theme

Version 5.1.8 - May.11th ‘12
===============================================
Added : two more footer layout type
Added : grayscale image effect support
Added : Archives and Categories shortcode
Added : attribute for disable title in blog shortcode
Added : More columns for portfolio shortcode
Added : category__and and category__not_in attribute for blog shortcode
Added : desc length attribute for blog shortcode
Added : desc length attribute for shortcode shortcode
Added : posts attribute for popular_posts shortcode
Added : posts attribute for  recent_posts shortcode
Added : option for disable category title of links shortcode
Added : option for changing sortable portfolio show text
Added : attribute for disable all tab in sortable portfolio
Added : Default Custom Sidebar Settings
Added : Align option for framed box
Added : NextGEN Gallery integration
Added : option for changing image alt of Advertisement 125 Widget
Added : Exclude post type From Search option
Added : woocommerce plugin compatibility imporvement
Added : position y for background options
Added : option for disable featured image in blog archives page
Added : option for changing layout of search result page
Added : option for change nvio slider loading background color
Added : option for change anything slider loading background color
Added : custom js option
Added : slider sources order support
Added : style for wpmu signup page
Added : Display full option for search result page
Added : Search Nothing Found Text option
Added : google font support
Added : new Feature Header Type: Title & SlideShow
Added : polylang compatibility
Added : Right Float option for feature image type
Added : order support for meta information of blog post
Added : Contact Form message automatic fade after sending 
Added : Portuguese Translation
Added : Use Complex CSS Class Name option
Fixed : woocommerce plugin css class name conflict issue
Fixed : portfolio ajax without sortable lightbox issue
Fixed : related portfolio list issue
Fixed : search widget title can't be translated by wpml issue
Fixed : 3d slider with wpml issue
Fixed : breadcrumbs blog link with wpml issue
Fixed : blog exclude categorys not working in post list widgets issue
Fixed : Contact Form not working issue
Fixed : Fontface issue
Fixed : adaptive height blog feature image resize issue
Fixed : Next & Prev Buttons for nivo shortcode
Fixed : cache issue for multi site
Fixed : stickyfooter issue
Fixed : gmap marker not show on footer issue
Updated : Mediaelement js
Updated : AnythingSlider js
Updated : tweet js
Updated : timthumb script
updated : chosen script

Version 5.1.7 - Feb.1th ‘12
===============================================
Added : Disable Colorbox option
Added : Offset attribute for blog post shortcode
Fixed : Anything slider shortcode animationTime attribute issue
Fixed : Portfolio shortcode current attribute issue
Fixed : Related Post widget title length issue
Fixed : Navigation hover color issue

Version 5.1.6 - Jan.21th ‘12
===============================================
Added : exclude categories for blog page option
Added : randomStart option for nivo slider
Added : picture frame shortcode align option
Added : title lenth option for post list widget
Added : make accordion can close by default
updated : Tweet js
updated : Nivo slider js
updated : Mediaelement js
updated : Breadcumbs
Fixed : Gmap line break issue
Fixed : Image shortcode auto height issue

Version 5.1.5 - Jan.12th ‘12
===============================================
Added : check the capability of user for show theme admin bar
Fixed : shortcode generator missing on homepage option page issue
Fixed : Related portfolio plugin empty category issue
Fixed : Monthly Archive feature title issue
Fixed : boxed layout background color option issue
Changed : use non-timthumb reszie function by default

Version 5.1.4 - Jan.6th ‘12
===============================================
Fixed : Upgrade issue

Version 5.1.3 - Jan.5th ‘12
===============================================
Added : Spam check for contact form plugin
Fixed : Combine js issue with Wordpress 3.3

Version 5.1.2 - Jan.4th ‘12
===============================================
Added : Wordpress 3.3.1 Compatibility 
Added : Frame layout for blog post
Added : More control for Meta informations in Header Area of Post Page
Added : automatic replace line break with br tag for gmap shortcode generator
Added : Boxed layout Background Color option
Fixed : Description issue for blog posts widgets
Fixed : Upload option issue
Fixed : Color selector issue in some browse
Fixed : Double breadcrumbs issue

Version 5.1.1 - Jan.1th ‘12
===============================================
Added : Related Portfolio List widget
Added : Relevanssi plugin Compatibility
Added : Top Level Menu Background Color option
Added : Breadcrumbs Text Size option
Fixed : Custom Favicon issue
Fixed : background stretch problem
Fixed : Picture frame shortcode generator issue


Version 5.1 - Dec.24th ‘11
===============================================
Added : Non-timthumb image resize support
Added : Clear Cache option
Added : Scroll to top button
Added : WooCommerce plugin compatibility
Added : portfolio_list shortcode
Added : Weekly Archive feature header support
Added : add nofollow rel to read more of posts for seo
Changed : Layout option for pages. 
Fixed : breadcrumbs double post title issue

Version 5.0.6 - Dec.17th ‘11
===============================================
Fixed : wp_editor issue with wp 3.3
Fixed : upgrade issue with wp 3.3
Fixed : unwanted slash of archives Feature text issue 
Added : doubleclickzoom option for gmap
Added : background-attachment option for backgrounds

Version 5.0.5 - Dec.4th ‘11
===============================================
Fixed : page_css_class filter argument issue

Version 5.0.4 - Dec.3th ‘11
===============================================
Added : Portfolio Related and Recent Module option
Added : Portfolio About Author option
Added : Navigation Parent Arrow option
Added : Media Uploader Compatibility for Wordpress 3.3 
Fixed : Lightbox gallery for featured image in post/portfolio page issue
Fixed : Nivo slider issue


Version 5.0.3 - Nov.22th ‘11
===============================================
Added : Sortable portfolio with page navigation support
Added : Portfolio ajax navigation support
Added : Shortcode support in comment option
Added : Show featured image on feeds option
Added : Featured Header Text customization options
Added : Boxed layout
Added : Lightbox gallery for featured image in post/portfolio page
Added : links widget shortcode
Added : Swedish Translation
Fixed : Anything slider Animation/delay Time issue
Fixed : Daniel cufon font issue

Version 5.0.2 - Nov.15th ‘11
===============================================
Added : Accordion slider text size options
Added : Sticky Footer option
Added : Portfolio slider source
Fixed : Nivo slider caption title issue
Fixed : Homepage editor issue on wp 3.3

Version 5.0.1 - Nov.8th ‘11
===============================================
Added : Nofollow attribute for button shortcode
Fixed : Lightbox navigation issue
Fixed : Upgrade with ftp issue

Version 5.0 - Nov.7th ‘11
===============================================
Added : Upgrade theme option
Added : Theme menu on Admin bar
Added : Right float image blog layout
Added : Font size option for Portfolio Description text
Added : Option for Portfolio Read More text Display as button
Added : Advanced divider shortcode
Added : Open attribute for toggle shortcode
Added : Sub current menu's color option
Fixed : Timthumb issue in some case
Fixed : Lightbox shortcode generator issue
Fixed : Gmap issue when marker is disabled
Fixed : Categories widget with count css issue
Fixed : Header and menu missing issue in safari, iphone & ipad issue
Updated : Documentation


Version 4.0.3 - Sep.28th ‘11
===============================================
Added : blog featured image below title option 
Added : Tab content color options
Added : Page Content text field color option
Added : image link lightbox option
Added : search widget shortcode
Added : shortcode generator for gallery
Added : align attribute for slidershow shortcodes
Fixed : nivo slider issue
Fixed : Anything slider with video issue
Fixed : 3d sldier issue
Fixed : Columns layout shortcode issues
Fixed : Blog shortcode page navigation on front page issue
Fixed : Framed box shortcode margin issue
Fixed : google map shortcode with align attribute issue
Fixed : iframe shortcode frameborder issue
Fixed : portfolio widget with category issue
Fixed : shortcode generator disappear issue in some situation

Version 4.0.2 - Sep.4th ‘11
===============================================
Added : Google Plus social icon
Fixed : Disable Read More button in Full Post view
Fixed : Gmap issue
Fixed : Audio shortcode width & height attribute issue
Fixed : Raw text appear issue
Fixed : Font family option issue
Fixed : Remove post thumbnail round border style

Version 4.0.1 - Aug.31th ‘11
===============================================
Added : Blog Post title color option
Added : make Blog Read More display as button
Fixed : Sub level menu active color issue
Fixed : Blog Exclude Categories issue
Fixed : html5 video autoplay issue with ie
Fixed : Sitemap Page “Edit” link issue
Fixed : Shortcode Generator issue in ie
Fixed : Invalid argument error message
Fixed : Blog post title color display as read
Fixed : Slideshow height issue

Version 4.0 - Aug.30th ‘11
===============================================
Added : Display Site Description with Custom Logo
Added : Anything slider shortcode
Added : link support for nivo slideshow shortcode
Added : Audio shortcode
Added : blip.tv video shortcode
Added : Portfolio with sidebar layout
Added : Portfolio list widget
Added : multiple source for slideshow
Added : Author query support for post list widget
Added : Batch upload images support for Portfolio Gallery
Added : Blog meta link color options
Added : a lot improvement in the backend
Fixed : Navigation hover color issue
Fixed : Timthumb script secure issue
Fixed : skin initialization issue
Updated : Anything slider script
Updated : Gmap scripts from using v2 api to v3
Changed : change videojs to mediaelementjs
Changed : New shortcode generator

Version 3.0.6.1 - July.1th ‘11
===============================================
Fixed : Gmap issue
Fixed : Slideshow Source select issue
Fixed : Author name issue for Authors List Widget
Added : Caption for Anything Slider with Posts Source
Checked : Wordpress 3.1.4 Compatibility
Checked : Ready for Wordpress 3.2

Version 3.0.6 - Jun.27th ‘11
===============================================
Fixed : Homepage blog naviagtion issue
Fixed : Youtube layout issue
Fixed : Color option issue
Added : define number of items to show on Slideshow
Added : make title linkable for Portfolio Shortcode
Added : make featured image open in lightbox on blog index page
Added : Reset theme option
Added : Blog column layout for archive pages
Added : css sprites optimization

Version 3.0.5 - Jun.3th ‘11
===============================================
Added : Wordpress 3.1.3 Compatibility
Added : Child theme support
Added : Nesting style for Custom Menu widget
Added : Button color option for contact form and search widget
Added : Box animations for nivo slider
Added : Order parameter for portfolio shortcode
Added : Recent posts support for slideshow
Added : footer text field color option
Added : General layout option
Added : lightboxtitle parameter for gallery shortcode
Fixed : Blog post next & prev navigation issue
Fixed : Front page blog post navigation issue

Version 3.0.4 - Apr.26th ‘11
===============================================
Fixed : Tabs initializtion issue
Fixed : Cufon options issue

Version 3.0.3 - Apr.21th ‘11
===============================================
Added : Wordpress 3.1.1 Compatibility 
Added : Danish Translation
Added : Grid layout for blog multiple columns
Added : Initial Tab for Tabs
Fixed : Tabs with history issue
Fixed : Insert image of shortcode generator issue
Fixed : Font options issue
Fixed : Hompage not saving issue

Version 3.0.2 - Apr.10th ‘11
===============================================
Fixed : Navigation overlap issue
Fixed : Gmap with tab issue
Fixed : Frontpage blog navigation issue
Fixed : Blog featured image width issue
Fixed : Wpml homepage slideshow issue
Added : Fix option for 'Enable Read More' button issue
Added : Fix option for 'Disable Breadcrumbs' button issue

Version 3.0.1 - Apr.1th ‘11
===============================================
Fixed : Blog meta bar issue
Fixed : Accordion default is closed issue
Fixed : Contact form issue
Fixed : Cufon replacement for sortable portfolio title in ie issue
Fixed : autoheight for image shortcode issue
Fixed : Display full content with shortcode support for blog posts shortcode
Fixed : Sub footer link color issue
Fixed : Homepage slideshow category issue
Fixed : Nivo Slideshow shortcode issue
Added : Id attribute for flash video shortcode

Version 3.0 - Mar.25th ‘11
===============================================
Added : WP 3.1 Compatibility, IE 9 and FF 4.0 Compatibility
Added : Nivo Slideshow Shortcode added with 10 attributes
Added : Custom Background loader for header, feature, page body and footer areas with adjustable attributes (position, tiling)
Added : 45 Fontface Fonts
Added : Online Documentation
Added : Table width attribute to table shortcode
Added : History attribute to Tabs Shortcode
Added : History attribute to Mini Tabs Shortcode
Added : Download links attribute to HTML 5 video shortcode
Added : 10 new attributes to Google video shortcode
Added : 4 new attributes to Vimeo Video Shortcode
Added : 3 new attributes to Dailymotion shortcode
Added : Thumbnail Height attribute to portfolio shortcode
Added : Advanced Display Description Attribute to portfolio shortcode
Added : Lightbox Title attribute w/3 selectable settings to portofolio shortcode
Added : # of Columns Attribute w/6 selectable settings to Blog Shortcode
Added : Height of Feature Images attribute to Blog Shortcode
Added : Description attribute to Blog Shortcode
Added : 2 more color controls for header area
Added : 4 more color controls for page area
Added : mobile slider replacement control for 3D slider
Added : 2 more Single Blog Image controls
Added : 5 more Portfolio controls
Added : excluded pages from 404 page as navigation menu.
Added : Link attribute for Contact info widget
Added : Link target for Advertisement 125 Widget
Changed : tri-state toggle for 'Disable Breadcrumbs option' in 'Page General Options' Module.
Changed : tri-state toggle for 'Enable Read More' in 'Portfolio Item Options' Module.

Version 2.2.3 - Dec.18th ‘10
===============================================
Fixed : Page navigation on dedicated blog page issue

Version 2.2.2 - Dec.16th ‘10
===============================================
Fixed : Custom homepage with custom sidebar issue
Added : Custom homepage with custom feauture color
Added : Norwegian translation
Added : target for portfolio more link 

Version 2.2.1 - Dec.15th ‘10
===============================================
Fixed : Contact form shortcode issue
Fixed : Homepage with slider issue

Version 2.2 - Dec.14th ‘10
===============================================
Added : Header widget area
Added : Sub Footer widget area
Added : Homepage with introduce text
Added : Author page
Added : Author widget
Added : Avatar and multi user support for twitter widget
Added : SlideShow for pages & posts
Added : multi image for portfolio item
Added : more control for portfolio shortcode
Added : iframe shortcode
Added : lightbox for gallery shortcode
Added : the ability to Import/Export all the theme options data
Added : javascript & css optimizer to speed up the site
Added : video size option for portfolio item
Added : font line-height option
Added : color option for Nivo slider caption
Added : Background color option for Anything slider
Added : color option for Tab & Accordion
Added : link target for slideshow item
Added : Traditional Chinese, Russian, Dutch, Spanish translation
Fixed : Capitalization for Social widget
Fixed : Color for contact info in footer
Fixed : translation for contact form
Fixed : ie6 compatibility
Changed : Move Cache folder to the top
Updated : update videojs to version 2.0.2
Updated : update nivo slider to version 2.3
Updated : update ColorBox to version 1.3.15
Updated : update cufon-yui to version 1.09i
Updated : update TimThumb to version 1.19

Version 2.1 - Nov.10th ‘10
===============================================
Added : close button option for lightbox shortcode
Added : next & previous button support for all types of portfolio item
Changed : remove wpautop function for column shortcode
Fixed : video shortcode fallback flash player overlap issue
Fixed : image shortcode issue

Version 2.0 - Nov.9th ‘10
===============================================
Added : Anything slider
Added : sort support for portfolio
Added : next & previous button for portfolio item single page
Added : Portfolio thumbnail link to external link
Added : Link icon for framed image
Added : shortcode for sitemap item
Added : Lightbox shortcode
Added : google chart shortcode
Added : group support for flickr widget
Added : target for icon link shortcode
Added : align option for google map shortcode
Added : title option for vimeo video shortcode
Added : rollover color option for button shortcode
Added : color option for portfolio sortable header
Added : blog meta information options
Added : height control for 3d slider
Added : option for global disable featured bar
Added : Styles for search widget in footer
Added : full width featured image support
Added : auto adjust Height option for image shortcode
Added : stop at end option for nivo slider
Added : translation support for copyright, twitter widget, contact widget
Added : Simplified Chinese, Italian, Deutsch, Japanese, Brazilian, Slovenian Translation
Changed : localize html5.js file
Changed : table shortcode to make it compatible with WP-Table Reloaded plugin
Changed : remove loading image text on 3d slider
Changed : the gap between sidebar & featured area when disable breadcrumbs
Changed : remove alt for portfolio featured image
Fixed : sortable portfolio animation issue
Fixed : disable portfolio "read more" in one column
Fixed : ie layout issue
Fixed : the issue that access homepage directly with wpml
Fixed : html5 shortcode issue with ie
Fixed : Full Blog Posts with <!--more--> tag issue in blog index page

Version 1.9.1 - Oct.26th ‘10
===============================================
Added : Video shortcodes
Added : Font family option
Added : color option for h1~h6
Added : Advertisement 125 widget
Added : Style for Footer Tab shortcode
Added : Rounded corner option for framed box
Added : Latin-1 Supplement glyphs for cufon replacement
Added : Layout for portfolio item
Added : Layout for blog post
Added : Compatible with Google XML Sitemaps
Added : Portfolio for sitemap page
Fixed : Blog post filter issue
Fixed : Column shortcode issue
Fixed : Table shortcode issue
Fixed : Cufon replacement delay on ie

Version 1.8 - Oct.19th ‘10
===============================================
Added : WPML support
Added : Custom style sheet option
Added : toggle option for blog post feature image
Added : specific category for posts widgets
Added : Related posts widget
Added : Left float image for Blog posts list
Added : Timthumb Support for Multisite Domain Mapping
Added : Option for the bottom of Logo
Added : Link target for buttons
Fixed : Footer contact info widget icon issue.
Fixed : Blog posts shortcode meta linefeed issue.
Fixed : Google map widget height issue.
Changed : Blog posts list title warpped in h2

Version 1.7 - Oct.15th ‘10
===============================================
Added : Translate Support
Added : Featured Image for Portfolio Item
Added : Next-prev buttons for video portfolio Item
Added : Height control for Portfolio item thumbnail
Added : Align Control for Button
Added : Cellphone for Contact Info Widget
Added : Control for the gap between posts
Added : Custom icon for social widget
Added : Icons for Ozh＊ Admin Drop Down Menu
Fixed : Refresh cufon replacement after re-sort the portfolio items
Fixed : Left Sidebar for page
Changed : Reduce the gap between content and footer

Version 1.6.1 - Oct.12th ‘10
===============================================
Added : Font size Control for sidebar widget title
Added : Sort support for Slider item
Added : different feature background color per page
Added : Previous-next navigation for post
Added : height option for accordion slider
Fixed : multi-category for portfolio
Fixed : Preview button error
Changed : make sidebar divider long enough

Version 1.5.1 - Oct.8th ‘10
===============================================
Added : Shortcode for Blog posts that you can display posts on frontpage
Added : Captions for Nivo Slider
Added : Shortcode Generator for Homepage content editor
Added : left sidebar layout for blog
Added : Slider item link to portfolio
Added : More control for footer
Changed : Styles for sidebar widgets
Changed : Layout of subfooter to avoid linefeed
Fixed : Homepage Content Formatting issue
Fixed : Meta bar display issue in single post

Version 1.4 - Oct.7th ‘10
===============================================
Added : Control for Nivo Slider height (click)
Added : Control for Blog Featured Image (click)
Fixed : Category archive error in PHP4
Fixed : Category archive error when use default permalink

Version 1.3 - Oct.6th ‘10
===============================================
Added : Added more control for styled framed box shortcode.
Changed : compatible with PHP4